FROM ghcr.io/astral-sh/uv:python3.10-bookworm-slim

WORKDIR /container

COPY pyproject.toml uv.lock ./

RUN uv sync --no-install-project

COPY . .
