from typing import Any

import pytest

from poetry_git_version_plugin.config import GitVersionPluginConfig, ReleaseTypeEnum
from poetry_git_version_plugin.services import VersionService


@pytest.fixture()
def version_service_config():
    return {'release_type': ReleaseTypeEnum.dev}


@pytest.fixture
def git_version_service(version_service_config: dict[str, Any]):
    config = GitVersionPluginConfig(**version_service_config)
    return VersionService(config)
